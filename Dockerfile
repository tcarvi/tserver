FROM alpine/git:latest
WORKDIR /
RUN git clone --recurse-submodules https://gitlab.com/tcarvi/tserver.git

FROM eduardoleal1981/go-server:1.0.1

# For website audiomedicus.com.br
COPY --from=0 /tserver/audiomedicus/css /public/audiomedicus/css
COPY --from=0 /tserver/audiomedicus/docs /public/audiomedicus/docs
COPY --from=0 /tserver/audiomedicus/img /public/audiomedicus/img
COPY --from=0 /tserver/audiomedicus/js /public/audiomedicus/js
COPY --from=0 /tserver/audiomedicus/index.html /public/audiomedicus/index.html

# For website naporteira.com.br
COPY --from=0 /tserver/naporteira/css /public/naporteira/css
COPY --from=0 /tserver/naporteira/docs /public/naporteira/docs
COPY --from=0 /tserver/naporteira/img /public/naporteira/img
COPY --from=0 /tserver/naporteira/js /public/naporteira/js
COPY --from=0 /tserver/naporteira/index.html /public/naporteira/index.html

# For website nofrete.com.br
COPY --from=0 /tserver/nofrete/css /public/nofrete/css
COPY --from=0 /tserver/nofrete/docs /public/nofrete/docs
COPY --from=0 /tserver/nofrete/img /public/nofrete/img
COPY --from=0 /tserver/nofrete/js /public/nofrete/js
COPY --from=0 /tserver/nofrete/index.html /public/nofrete/index.html

# For website noluar.com.br
COPY --from=0 /tserver/noluar/css /public/noluar/css
COPY --from=0 /tserver/noluar/docs /public/noluar/docs
COPY --from=0 /tserver/noluar/img /public/noluar/img
COPY --from=0 /tserver/noluar/js /public/noluar/js
COPY --from=0 /tserver/noluar/index.html /public/noluar/index.html

# For website tcarvi.com.br
COPY --from=0 /tserver/tcarvi/css /public/tcarvi/css
COPY --from=0 /tserver/tcarvi/docs /public/tcarvi/docs
COPY --from=0 /tserver/tcarvi/img /public/tcarvi/img
COPY --from=0 /tserver/tcarvi/js /public/tcarvi/js
COPY --from=0 /tserver/tcarvi/index.html /public/tcarvi/index.html
#!/bin/bash

# Docker configuration. (apenas primeiro uso do docker)
# sudo groupadd --system docker
# sudo chgrp docker ./*.*
# sudo chmod 540 buildVersioning.sh
# sudo chmod 540 minorVersioning.sh
# sudo chmod 540 majorVersioning.sh
# sudo chmod 540 updateTcarvi.sh

# Exclusão de imagem antiga e de imagems criadas indevidamente
# OLD_VERSION_NUMBERS="$(cat VERSION)"
# docker rmi -f gcr.io/tcarvi/tcarvi-webserver:$OLD_VERSION_NUMBERS || true
# clean untagged images
# docker system prune --force

# Versioning
# ./majorVersioning.sh
# ./minorVersioning.sh
# ./buildVersioning.sh

# VERSION_NUMBERS="$(cat VERSION)"

# Build
# docker build --rm --file tcarvi/devops/docker/Dockerfile --tag gcr.io/${PROJECT_ID}/tcarvi-webserver:${VERSION_NUMBERS} .
# clean untagged images
# docker system prune --force
# docker push gcr.io/${PROJECT_ID}/tcarvi-webserver:${VERSION_NUMBERS}